# Fancy Checkbox

A simple piece of code to re-design checkboxes.


## Include the CSS

Include `fancy-checkbox.css` in the `<head>` of your HTML.

```
<link rel="stylesheet" href="/path/to/your/fancy-checkbox.css" />
```


## Use the following HTML structure:

```
<div class="fancy-checkbox">
	<input type="checkbox" id="chkbox-1" />
	<label for="chkbox-1">This is an amazing option</label>
</div>
```
